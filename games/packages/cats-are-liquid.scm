;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Julien Lepiller <julien@lepiller.eu>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages cats-are-liquid)
  #:use-module (nonguix build-system binary)
  #:use-module (guix download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (games game-local-fetch)
  #:use-module (ice-9 match)
  #:use-module (nonguix licenses))

(define icon
  (origin
    (method url-fetch)
    (uri "https://66.media.tumblr.com/avatar_d2a6378eef39_128.pnj")
    (sha256 (base32 "1i7w8c335qnjknlf5z9nhhmhdp0n54f65pb4awrcm2wix6xypqyv"))))

(define-public cats-are-liquid-a-light-in-the-shadows
  (package
    (name "cats-are-liquid-a-light-in-the-shadows")
    (version "0")
    (source
     (origin
      (method game-local-fetch)
      (uri "CatsAreLiquidLinux.zip")
      (sha256
       (base32
        "017pdi37nb7i3p829v5lw01glsjqjvvxci2vshk8wn8g68f99xdn"))))
    (supported-systems '("i686-linux"))
    (build-system binary-build-system)
    (arguments
     `(#:patchelf-plan
       `(("CatsAreLiquidLinux.x86"
          ("libc" "gcc:lib" "eudev" "gdk-pixbuf" "glib" "gtk+-2" "libx11"
           "libxcursor" "libxext" "libxi" "libxinerama" "libxrandr" "libxscrnsaver"
           "libxxf86vm" "mesa" "pulseaudio" "zlib"))
         ("CatsAreLiquidLinux_Data/Plugins/x86/ScreenSelector.so"
          ("libc" "gcc:lib" "gtk+-2")))
       #:install-plan
       `(("CatsAreLiquidLinux.x86" "share/cats-are-liquid/")
         ("CatsAreLiquidLinux_Data" "share/cats-are-liquid/"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'make-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (wrapper (string-append out "/bin/cats-are-liquid"))
                    (binary (string-append out "/share/cats-are-liquid/CatsAreLiquidLinux.x86")))
               (make-wrapper
                 wrapper binary
                 #:skip-argument-0? #t
                 `("PATH" ":" prefix
                   (,(string-append (assoc-ref inputs "pulseaudio") "/bin")))))
             #t))
         (add-after 'install 'make-desktop-entry
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (make-desktop-entry-file
                 (string-append out "/share/applications/cats-are-liquid.desktop")
                 #:name "Cats are Liquid - A Light in the Shadows"
                 #:icon (assoc-ref inputs "icon")
                 #:exec (string-append out "/bin/cats-are-liquid")
                 #:categories '("Application" "Game")))
             #t)))))
    (inputs
     `(("eudev" ,eudev)
       ("gcc:lib" ,gcc "lib")
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("glib" ,glib)
       ("gtk+-2" ,gtk+-2)
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("libxi" ,libxi)
       ("libxinerama" ,libxinerama)
       ("libxrandr" ,libxrandr)
       ("libxscrnsaver" ,libxscrnsaver)
       ("libxxf86vm" ,libxxf86vm)
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)
       ("zlib" ,zlib)))
    (native-inputs
     `(("unzip" ,unzip)
       ("icon" ,icon)))
    (home-page "https://lastquarterstudios.itch.io/cats-are-liquid-a-light-in-the-shadows")
    (synopsis "2D platformer about a cat with the ability to transform into liquid")
    (description "Cats are Liquid - A Light in the Shadows is a 2D platformer
about a cat with the ability to transform into liquid.

The game has 90 levels, that are spread across 9 different worlds.  It has a
minimalistic but colorful style.  Along the way the game introduces new
mechanics, like flying and summoning bombs to break down walls.

The story is about a cat whose owner locked in a set of rooms.  She desperately
wants to get out, but the rooms just keep continuing.  Along the way the cat
meets a new \"friend\" and gains new abilities.  The story is told through
small in-game text pieces.")
    (license (undistributable "No License"))))

(define-public cats-are-liquid-a-better-place
  (package
    (name "cats-are-liquid-a-better-place")
    (version "0")
    (source
     (origin
      (method game-local-fetch)
      (uri "CaL-ABP-Linux.zip")
      (sha256
       (base32
        "11vj3z068c4lkhigx36wah3rmicxfjwxv11h8a98ck0alylc4smx"))))
    (supported-systems '("x86_64-linux"))
    (build-system binary-build-system)
    (arguments
     `(#:patchelf-plan
       `(("CaL-ABP-Linux.x86_64"
          ("libc" "gcc:lib" "eudev" "gdk-pixbuf" "glib" "gtk+-2" "libx11"
           "libxcursor" "libxext" "libxi" "libxinerama" "libxrandr" "libxscrnsaver"
           "libxxf86vm" "mesa" "pulseaudio" "zlib"))
         ("CaL-ABP-Linux_Data/Plugins/x86_64/ScreenSelector.so"
          ("libc" "gcc:lib" "gtk+-2")))
       #:install-plan
       `(("CaL-ABP-Linux.x86_64" "share/cats-are-liquid/")
         ("CaL-ABP-Linux_Data" "share/cats-are-liquid/"))
       #:phases
       (modify-phases %standard-phases
         (add-before 'install 'fix-permissions
           (lambda _
             (chmod "CaL-ABP-Linux.x86_64" #o755)
             #t))
         (add-after 'install 'make-wrapper
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (wrapper (string-append out "/bin/cats-are-liquid-abp"))
                    (binary (string-append out "/share/cats-are-liquid/CaL-ABP-Linux.x86_64")))
               (make-wrapper
                 wrapper binary
                 #:skip-argument-0? #t
                 `("PATH" ":" prefix
                   (,(string-append (assoc-ref inputs "pulseaudio") "/bin")))))
             #t))
         (add-after 'install 'make-desktop-entry
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (make-desktop-entry-file
                 (string-append out "/share/applications/cats-are-liquid.desktop")
                 #:name "Cats are Liquid - A Better Place"
                 #:icon (assoc-ref inputs "icon")
                 #:exec (string-append out "/bin/cats-are-liquid-abp")
                 #:categories '("Application" "Game")))
             #t)))))
    (inputs
     `(("eudev" ,eudev)
       ("gcc:lib" ,gcc "lib")
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("glib" ,glib)
       ("gtk+-2" ,gtk+-2)
       ("libx11" ,libx11)
       ("libxcursor" ,libxcursor)
       ("libxext" ,libxext)
       ("libxi" ,libxi)
       ("libxinerama" ,libxinerama)
       ("libxrandr" ,libxrandr)
       ("libxscrnsaver" ,libxscrnsaver)
       ("libxxf86vm" ,libxxf86vm)
       ("mesa" ,mesa)
       ("pulseaudio" ,pulseaudio)
       ("zlib" ,zlib)))
    (native-inputs
     `(("unzip" ,unzip)
       ("icon" ,icon)))
    (home-page "https://lastquarterstudios.itch.io/cats-are-liquid-a-better-place")
    (synopsis "2D platformer about a cat with the ability to transform into liquid")
    (description "You play as the cat, in a place created just for you and your
friends.  You get to go on a nice adventure with them where nothing goes wrong
and everything is perfect, as long as your friends stay there, right by your
side.

Over the course of the game, you get to explore 120 rooms and discover new
abilities like the hookshot and light burst.

A world editor is included.")
    (license (undistributable "No License"))))
