(define-library (games packages factorio)
                (export factorio factorio-next)
                (import (scheme base)
                        (scheme lazy)
                        (web uri)
                        (guix base16)
                        (guix gexp)
                        (guix packages)
                        (nonguix build binary-build-system)
                        (nonguix build-system binary)
                        (nonguix download)
                        (nonguix licenses)
                        (games utils)
                        (gnu packages base)
                        (gnu packages gl)
                        (gnu packages gnome)
                        (gnu packages linux)
                        (gnu packages package-management)
                        (gnu packages pulseaudio)
                        (gnu packages xorg))
                (begin
                  (define (fetch uri . args)
                    (let ((conf (read-config (string-append
                                              "FETCH FAILED
============

Please ensure you have set the username and token in `"
                                              (guix-gaming-channel-games-config)
                                              "'.
Your token can be seen at https://factorio.com/profile (after logging in).  It is
not as sensitive as your password, but should still be safeguarded.  There is a
link on that page to revoke/invalidate the token, if you believe it has been
leaked or wish to take precautions.

Example content for the file:

```scheme
((game1 ...)
 ...
 (factorio .
   ((username . \"johnsmith\")
    (token    . \"c40c9636e1ba61261a50e49a035fff1d\")))
 ...
 (gameN ...))
```")
                                             'factorio)))
                      (apply unredistributable-url-fetch
                             (string-append uri "?username="
                                            (uri-encode (cdr (assq 'username
                                                                   conf)))
                                            "&token="
                                            (uri-encode (cdr (assq 'token conf))))
                             args)))

                  (define (alpha version hash)
                    (origin
                      (uri (string-append "https://factorio.com/get-download/"
                            (uri-encode version) "/alpha/linux64"))
                      (method fetch)
                      (sha256 (base16-string->bytevector hash))))

                  (define (headless version hash)
                    (origin
                      (uri (string-append "https://factorio.com/get-download/"
                            (uri-encode version) "/headless/linux64"))
                      (method unredistributable-url-fetch)
                      (sha256 (base16-string->bytevector hash))))

                  (define factorio
                    (package
                      (name "factorio")
                      (version "1.1.100")
                      (source
                       (origin
                         (uri (string-append
                               "https://factorio.com/get-download/"
                               (uri-encode version) "/alpha/linux64"))
                         (method fetch)
                         (sha256
                          (base16-string->bytevector
                           "8c1708e90d85eacd1641c93565ff34a596ff742412c0d23f69a99b9e460f997a"))))
                      (build-system binary-build-system)
                      (arguments
                       `(#:patchelf-plan '(("bin/x64/factorio" ("alsa-lib"
                                                                "glibc"
                                                                "libx11"
                                                                "libxcursor"
                                                                "libxext"
                                                                "libxinerama"
                                                                "libxrandr"
                                                                "mesa"
                                                                "pulseaudio")))
                         #:install-plan '(("bin/x64/factorio"
                                           "libexec/factorio")
                                          ("data" "share/factorio")
                                          ("doc-html" "share/doc/factorio"))
                         #:validate-runpath? #f
                         #:strip-binaries? #f
                         #:phases ,#~(modify-phases %standard-phases
                                       (delete 'make-dynamic-linker-cache)
                                       (add-after 'install 'wrap
                                         (lambda* (#:key outputs
                                                   #:allow-other-keys)
                                           (import (scheme base)
                                                   (scheme file))
                                           (let* ((out (assoc-ref outputs
                                                                  "out"))
                                                  (wrapper (string-append out
                                                            "/bin/factorio")))
                                             (mkdir (string-append out "/bin"))
                                             (with-output-to-file wrapper
                                               (lambda ()
                                                 (for-each write-string
                                                           (list "PATH=\""
                                                            #$zenity
                                                            "/bin:$PATH\" "
                                                            #$guix
                                                            "/bin/guix shell -CNE 'PATH|DISPLAY|XDG_DATA_DIRS|XDG_RUNTIME_DIR' --expose=/gnu \"--expose=$XDG_RUNTIME_DIR\" --expose=/dev --expose=/sys --expose="
                                                            out
                                                            "=/usr -- "
                                                            out
                                                            "/libexec/factorio \"$@\"\n"))))
                                             (chmod wrapper #o555)))))))
                      (inputs (list alsa-lib
                                    glibc
                                    guix
                                    libx11
                                    libxcursor
                                    libxext
                                    libxinerama
                                    libxrandr
                                    mesa
                                    pulseaudio
                                    zenity))
                      (synopsis "Factory building game")
                      (description
                       "Factorio is a game in which you build and maintain factories.

You will be mining resources, researching technologies, building infrastructure,
automating production and fighting enemies.  Use your imagination to design your
factory, combine simple elements into ingenious structures, apply management
skills to keep it working, and protect it from the creatures who don't really
like you.")
                      (license (undistributable
                                "https://factorio.com/terms-of-service"))
                      (home-page "https://factorio.com")))

                  (define factorio-next
                    (package
                      (inherit factorio)
                      (name "factorio-next")
                      (version "1.1.101")
                      (source
                       (origin
                         (uri (string-append
                               "https://factorio.com/get-download/"
                               (uri-encode version) "/alpha/linux64"))
                         (method fetch)
                         (sha256
                          (base16-string->bytevector
                           "d8918590c65b7e5f14cf93a88644f3422a759f375552a12e4dae42e23c83c81d"))))
                      (synopsis "Experimental release of Factorio")
                      (description "See `factorio'.")))))
